package net.way2java.demo;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SimpleLog {
    public static void main(String[] args) {
        log.debug("hello demo");
    }
}
